
reveal.js-exemple-statique

Version 2016-09-29 © Aful https://aful.org/  
distribué sous licence CC-BY-SA 4.0


---

<!-- .slide: data-background="#ff0000" -->

Exemple de présentation reveal.js basée uniquement sur des fichiers statiques,
sans aucune application en écoute (sans code serveur).

---

- Item 1 <!-- .element: class="fragment" data-fragment-index="2" -->
- Item 2 <!-- .element: class="fragment" data-fragment-index="1" -->

Du contenu

Encore du contenu

```javascript
let a;
let b = 'some string';
```

---

<!-- .slide: data-background="./image/bugs.jpg" -->

