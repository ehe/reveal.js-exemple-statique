reveal.js-exemple-statique
==========================

Exemple de présentation reveal.js basée uniquement sur des fichiers statiques,
sans aucune application en écoute (sans code serveur).

Utilisation
-----------

    $ npm install

et ensuite ouvrir le fichier `index.html` avec un navigateur web avec
le support de JavaScript activé.

C'est déployé et testable sur
https://aful.org/~madarche/reveal.js-exemple-statique/

